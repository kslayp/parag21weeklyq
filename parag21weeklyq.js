(function(){
    //答案庫在此
    var answerDic = {};
    answerDic["在救麗莎的雪原打倒吉爾伽斯後出現的先知是誰？"] = "斐埃爾";
    answerDic["聖光之錐使吉爾伽斯跪下後，需要接甚麼技能？"] = "審判之刃";
    answerDic["第一次遇見艾爾班騎士團成員是在哪個城鎮？"] = "塔拉";
    
    answerDic["第一次遇見艾薇琳的地方是？"] = "希德斯特雪原";
    answerDic["審判之刃的副技能，哪一個需要做使者武器3次？"] = "利刃洗禮";
    answerDic["艾爾班騎士團訓練所最高中級，獨自一人完成會獲得？"] = "更上一層的";
    
    answerDic["艾爾班騎士團所侍奉的神祇是？"] = "主神艾托恩";
    answerDic["亞特使用得武器為何？"] = "神聖雙手劍";
    answerDic["獨自前往希德爾特雪原並被米列希安及騎士團團員拯救的角色？"] = "麗莎";
    
    answerDic["若看見了金色的星星升向空中代表發生了甚麼事？"] = "有人成為了使徒";
    answerDic["在塔拉王城戰中保護的人是誰？"] = "艾蕾蒙";
    answerDic["神聖騎士團每週任務，獲得戰利品吧！是？"] = "使者之環";

    answerDic["最先學習到的騎士團技能是？"] = "聖盾庇護";
    answerDic["艾爾班騎士團巨魔像的頭盔毛色為藍色時，需要哪種攻擊才造成傷害？"] = "魔法";
    answerDic["G19拉赫王城最終決戰一次對付幾隻使徒？"] = "2";
    
    answerDic["第一次在塔拉王城門口遇到的騎士團成員為誰？"] = "亞特";
    answerDic["在與艾薇琳一同協力打吉爾伽斯時出來幫忙的角色為誰？"] = "托爾維斯";
    answerDic["第一次遇見的艾爾班騎士團成員是？"] = "亞特";

    let checkQuestion=function checkQuestion(){
        // Try to get question from <div class="question"> <div class="topic">
        var topicDiv = document.querySelector('.question .topic');
        if (topicDiv) {
            var currQues = topicDiv.innerHTML;
            var currAns = answerDic[topicDiv.innerHTML];

            var answerInLi = document.querySelectorAll('.question > ol > li');
            if (answerInLi && answerInLi.length > 0) {
                for(var i=0;i<answerInLi.length;i++) {
                    var extractedAnswer = answerInLi[i].innerHTML.match(/"([^']+)"/)[1];
                    if (extractedAnswer == currAns) {
                        answerInLi[i].style.color = "red";
                        answerInLi[i].textDecoration='none';
                    }
                    else {
                        answerInLi[i].style.color = "white";
                        answerInLi[i].textDecoration='line-through';
                    }
                }
            }
            else {
                console.log("問題為 \'" + currQues + "\' 但卻無法取得.question > ol > li");
            }
        } 
        else {
            console.log("找不到問題 .question .topic");
        }
    };
    
    var timer = setInterval(function(){ checkQuestion() }, 500);
    alert("已載入 (last update: 26 June 2018 1540)");
})();